'use strict'
module.exports = {
    header: {
        method: "POST",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json"
        }
    },
    api: {
        // base: "http://rap2api.taobao.org/app/mock/124114/",
        base: "http://192.168.137.1:8080/",
        tabledata: "wineapi/tablelist",
        winedata: "wineapi/winelist",
        tableupdate:"wineapi/tableupdate",
        orderadd:"wineapi/orderadd"
    }
}
// fetch('/xxx', {
//     method: 'post',
//     body: JSON.stringify({
//         username: '',
//         password: ''
//     })
// });