'use strict'

var queryString = require('query-string')
var _ = require('lodash')
const config = require('./config')

var request = {}

request.get = function (url, params) {
    if (params) {
        url += '?' + queryString.stringify(params)
    }
    return fetch(url)
        .then((response) => response.json())
}
request.post = function (url, body) {
    // var options = _.extend(config.header, {
    //     body: JSON.stringify(body)
    // })
    // console.log(options)
    console.log(body)
    url += '?' + `data=`+JSON.stringify(body)


    return fetch(url)
        .then((response) => response.json())
}
module.exports = request