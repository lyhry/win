import React, { Component } from 'react'
import { Text, View ,StyleSheet} from 'react-native'

export default class SmallLabel extends Component {
  constructor(props){
    super(props)
    this.state = ({

    })
  }
  render() {
    return (
      <View style={[styles.container,{width:this.props.width,borderColor:this.props.color}]}>
        <Text style={[styles.text,{color:this.props.color}]}>{this.props.content}</Text>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    borderRadius:1,
    borderWidth: 0.3,
    borderColor: 'red',
    height:16,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft:5
  },
  text: {
    color: 'red',
    fontSize:11
  }
 
});
