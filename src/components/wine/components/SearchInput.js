import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  FlatList,
  ScrollView,
  Image
} from "react-native";
import Icon from "react-native-vector-icons/AntDesign";
const Dimensions = require("Dimensions");
const { width } = Dimensions.get("window");
export default class SearchInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={styles.container}>
         <Icon name="search1" style={styles.headerIcon} size={25} />
            <TextInput
              style={styles.searchInput}
              placeholder="请输入商家、商品名称"
              onChangeText={text => this.setState({ text })}
            />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    // backgroundColor: "#F5FCFF",
    backgroundColor: "#fff",

    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerIcon: {
    // position:"relative"
    flex:1,
    marginLeft:5,
  },
  searchInput: {
    flex:10,
    height: width / 9,
    width: width / 1.2,
    // backgroundColor: "#ccc",
    marginLeft: 8
  },
})
