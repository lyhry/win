import React, { Component } from 'react'
import { Text, View ,StyleSheet} from 'react-native'
const Dimensions = require("Dimensions");
const { width } = Dimensions.get("window");

export default class Tips extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>已售完</Text>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    backgroundColor:'rgba(60,60,60,0.8)',
    width:150,
    height:width/2.6,
    borderRadius: 10,
    position:'absolute',
    alignItems: 'center',
    justifyContent: 'center',    
  },
  text: {
    color: '#fff',
    fontSize:16
  }
});
