import React, { Component } from "react";
import {
  StyleSheet,
  View,
  ScrollView,
} from "react-native";

// import Icon from "react-native-vector-icons/AntDesign";
import GoodsItem from "./GoodsItem";
import Footer from "./components/Footer";

const Dimensions = require("Dimensions");
const { width } = Dimensions.get("window");

// var arrs = require("./test.json");
const config = require('../service/config')
const request = require('../service/request')

export default class Goods extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data:[]
    }
  }
  componentWillUnmount = () => {
    this.setState = (state,callback)=>{
      return;
    };
  }
  componentDidMount(){
    let url = config.api.base + config.api.winedata
    let kindid = this.props.kindid
    let newarr = []
    let arrs

    request.get(url,{
    }).then(res => {
      // console.log(res)
      if(res.code == 'ok'){
        arrs = res.data
        for(let i=0;i<arrs.length;i++){
          if(kindid == arrs[i].kindid){
            newarr.push(arrs[i])
            this.setState({
              data:newarr
            })
          }
        }
      }else{
        alert('获取酒信息错误')
      }

    })
    .catch(err => {
      console.log(err)
    })

   
  }
  render() {
    let newdata = this.state.data
    return (
      <View>
        <ScrollView style={{ flex: 1 }} showsVerticalScrollIndicator={false}>
          <View style={styles.container}>
            {newdata.map((item, index) => {
              return (
                  <GoodsItem
                    addWine={this.props.addWine}
                    key={index}
                    data={item}
                  />
              );
            })}
            <Footer />
          </View>
        </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: width / 1.03,
    justifyContent: "flex-start",
    flexDirection: "row",
    flexWrap: "wrap"
  },
  header: {
    height: width / 6.5,
    width: width / 1.03,
    backgroundColor: "#F5FCFF",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    position: "absolute"
  },
  body: {
    marginTop: width / 7,
    flex: 1
  },
  goodsitem: {
    width: width / 2.06,
    height: 250,
    backgroundColor: "red",
    marginTop: 5,
    borderColor: "#000",
    borderWidth: 3
  }
});
