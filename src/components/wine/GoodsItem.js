import React, { Component } from 'react'

import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image
} from "react-native";
import Icon from "react-native-vector-icons/AntDesign";
import { Stepper } from '@ant-design/react-native';

import SmallLabel from './components/SmallLabel';
import Tips from './components/Tips';

const Dimensions = require("Dimensions");
const { width } = Dimensions.get("window");

export default class GoodsItem extends Component{
  constructor(props) {
    super(props);
    // this.addWine = this.addWine.bind(this)
    this.state = {
     data:null
    }
    this.addWine = this.addWine.bind(this)
    
  }
  componentDidMount(){
    this.setState({
      data:this.props.data
    })
  }
  addWine =()=>{
    let data = this.state.data
    data.count ++
    this.props.addWine(data)
  }
  render(){
    const readOnly = (
      <Stepper
        key="1"
        max={10}
        min={1}
        readOnly={false}
        defaultValue={1}
      />
    );
    let data = this.props.data
    // console.log(data)

    return(
      <View style={styles.container}>
        <View style={styles.goodsitem}>
        <Image source={{uri:data.img}} style={styles.imageView}></Image>
        {
          data.inventory != 0?null:<Tips></Tips>
        }
          <View style={styles.goodDescription}>
            <SmallLabel content = '自营' width={30} color='red'/>
            <SmallLabel content = '满赠' width={30} color='red'/>
            <SmallLabel content = '领券' width={30} color='red'/>
            <View style={{height:40,backgroundColor:"",marginTop:5}}>
            <Text style={{fontSize:14}}>{data.name}</Text>
            </View>
            <Text style={{fontSize:20,marginLeft:5,color:'red'}}>￥ {data.price}</Text>
            {
              data.inventory == 0?null:
              <TouchableOpacity onPress={this.addWine}>
              <Icon name='pluscircle' size={24} style={{marginLeft:60,color:'#00b894'}}/>
              </TouchableOpacity>
            }
          </View>
        </View>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container:{
    width:width/2.06,
    height:250,
    backgroundColor:'#fff',
    marginTop:5,
    borderColor: '#f1f2f6',
    borderWidth: 3,
    alignItems: 'center',
    justifyContent: "center",
    borderRadius: 6,
  },
  goodsitem:{
    width:width/2.25,
    height:240,
    alignItems:'center'
  },
  imageView:{
    width:width/2.5,
    height:width/2.5
  },
  goodDescription:{
    flexDirection:'row',
    flexWrap: 'wrap',
  }
})