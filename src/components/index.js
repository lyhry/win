import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,Button} from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';

const Dimensions = require("Dimensions");
const { width } = Dimensions.get("window");

import First from './first/index'
import Wine from './wine/index'
import Order from './order/index'


const RootStack = createStackNavigator(
  {
    First: First,
    Wine: Wine,
    Order:Order
  },
  {
    navigationOptions:{
    headerStyle: {
      backgroundColor: 'red',
      height:0
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
    }
  }
);
export default class App extends Component {
  constructor(props){
    super(props)
  }
  render() {
    return (
        <RootStack></RootStack>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
