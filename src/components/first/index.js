import React, { Component } from 'react'

import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity
} from "react-native";
import Icon from "react-native-vector-icons/AntDesign";

import Table from './table'
const config = require('../service/config')
const request = require('../service/request')

const Dimensions = require("Dimensions");
const { width } = Dimensions.get("window");
const Step = Steps.Step;
import { NoticeBar,Steps, Provider, Toast, } from '@ant-design/react-native';
export default class BasicTabsExample extends React.Component {
  static navigationOptions = {
    borderBottomColor: '#fff',
    borderBottom:0
  };
  constructor(props) {
    super(props);
    this.state = {
      tabledata:{}
    }
    this.nextPage = this.nextPage.bind(this)
  }
  onRef = (ref) => {
    this.child = ref
  }
  onFresh = () => {
    this.child.loadData()
  }
  componentWillUnmount = () => {
    this.setState = (state,callback)=>{
      return;
    };
  }
  nextPage = () =>{
    let tabledata = this.state.tabledata
    let tableid = this.state.tabledata.id
    let url = config.api.base + config.api.tableupdate
    if(tableid == null){
      Toast.info('请选择你的座位 !!!',1);
    }else{
      request.get(url,{
        tableid:tableid
      }).then((res) => {
        console.log(res.code)
        if(res.code == 'ok'){
          this.props.navigation.navigate('Wine', {
            tableid: tabledata.id,
            tabletext:tabledata.text,
          });
        }else{
          // alert('客户端更新桌子错误')
          Toast.info('客户端更新桌子错误')
        }
      })

      this.props.navigation.navigate('Wine', {
        tableid: tabledata.id,
        tabletext:tabledata.text,
      });
     
      //更新一次数据库
      
    }
  }
  Stable(val){
    this.setState({
      tabledata:val
    })
  }
  render() {
    {
      if(this.state.tabledata == null){
        return null
      }else{
        return (
          <Provider>
          <View style={styles.container}>
          <View style={styles.header}>
          {/* 父调用子方法 */}
          <TouchableOpacity style={{marginRight:5,flexDirection:'row',alignItems:"center"}} onPress={this.onFresh}>
            <Icon name='sync' size={18} style={{marginLeft:20}}></Icon>
          </TouchableOpacity>
          <TouchableOpacity style={{marginRight:5,flexDirection:'row',alignItems:'center'}} onPress={this.nextPage}>
            <Text style={{fontSize:15,marginRight:5}}>下一步</Text>
            <Icon name='right' size={15}></Icon>
          </TouchableOpacity>
            
          </View>
          <View style={styles.welcome}>
          <NoticeBar
              onPress={() => alert('click')}
              marqueeProps={{ loop: true, style: { fontSize: 14, color: 'red' } }}
              icon={<Icon name='sound' size={20} color='red'></Icon>}
            >
              好消息: 蒙古王酒属于浓香型白酒，采用传统酿造工艺，结合现代科学技术精酿而成，具有窖香浓郁，入口绵甜，余味悠长之特点，是草原百年佳酿质的升华。
              
            </NoticeBar>
          </View>
         
          <View style={styles.body}>
           <Table Stable={this.Stable.bind(this)} onRef={this.onRef}></Table>
          </View>
          </View>
          </Provider>
        );
      }
    }
   
  }
}
const styles = StyleSheet.create({
  container:{
    flex: 1,
    backgroundColor: "#F5FCFF",
    // justifyContent: "center",
    alignItems: "center"
  },
  header: {
        height: width / 6.5,
        width: width/1.03,
        backgroundColor: "#F5FCFF",
        alignItems: "center",
        justifyContent: 'space-between',
        flexDirection: "row",
        position: "absolute"
      },
      body: {
      marginTop: 10,
      flex:1,
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    // flexDirection:"row",
    alignItems:'center',
    justifyContent:"center",
    marginTop: width /6.5,

  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },

})