import React, { Component } from 'react'

import {
  ActivityIndicator,
  StyleSheet,
  Text,
  View,
  ScrollView,
} from "react-native";

import {Toast,Grid, List, Provider } from '@ant-design/react-native';
const config = require('../service/config')
const request = require('../service/request')

const Dimensions = require("Dimensions");
const { width } = Dimensions.get("window");

// const alldata = require('./test.json')

export default class Table extends React.Component {
  static navigationOptions = {
    borderBottomColor: '#fff',
    borderBottom:0
  };
  constructor(props) {
    super(props);
    this.state = {
      data:{},
      dataed:[],
      alldata:[]
    }
    this.selectTable = this.selectTable.bind(this)
  }
  loadData = () => {
    this.setState({
      dataed:[]
    })
    // alert(1)
    Toast.info('数据更新成功',1)
    let url = config.api.base + config.api.tabledata
    let alldata
    let self = this
    request.get(url).then(res => {
      // console.log(typeof(res))//object
      if(res.code == 'ok'){
        // console.log(res.data)
        alldata = res.data
        let dataed = this.state.dataed
        for(let i=0;i<alldata.length;i++){
          if(alldata[i].status == 0){
            dataed.push(alldata[i])
          }
        }
        self.setState({
          dataed:dataed,
          alldata:alldata
    
        })
      }else{
        alert(1)
      }
    })
    .catch(err => {
      console.log(err)
    })
  }
  componentDidMount = () =>{
    this.props.onRef(this)
    this.loadData()
  }
  componentWillUnmount = () => {
    this.setState = (state,callback)=>{
      return;
    };
  }
  selectTable(item){
    if(item.status == 1){
      Toast.info('该桌子已经有人 !!!',1);
      return;
    }else{
      this.setState({
        data:item
      })
      this.props.Stable(item)
    }
    // console.log(item)
   
  }
 
  render() {
    console.log(this.state.alldata)
    return (
      <View style={{width:width,flex:1}}>
      <ScrollView>
        {
          this.state.alldata.length == 0 ? <ActivityIndicator color="#2c3e50" /> :
        <View>
        <View style={[{ margin: 10 ,flexDirection:'row',alignItems:'center'}]}>
        {/* <ActivityIndicator></ActivityIndicator> */}
          <Text>{this.state.alldata?'请选择桌子:':null}  <Text style={{fontSize:18,color:'#2c3e50'}}>{!this.state.data ? null:this.state.data.text}</Text> </Text>
        </View>
        <View style={[{ padding: 10}]}>
          <Grid
          data={this.state.alldata}
          columnNum={3}
          isCarousel
          onPress={(item, index) => this.selectTable(item)}
        />
        </View>
        <View style={[{ margin: 10}]}>
          <Text style={{marginBottom:10}}>空桌子: </Text>
          {
            this.state.dataed.map((v,index) => {
              return(
                <List key={index}>
                  <List.Item>
                    <Text style={{color:'#636e72'}}>{v.text}</Text>
                  </List.Item>
                </List>
              )
            })
          }
        </View>
        <View style={[{ padding: 10}]}>
         
        </View>
        </View>
        }

      </ScrollView>
      </View>
    );
  }
}
// const styles = StyleSheet.create({
//   container:{
//     flex: 1,
//     backgroundColor: "#F5FCFF",
//     alignItems: "center"
//   },
//   body:{
//     marginTop: width /6.5,
//     flex:1,
//   },

// })