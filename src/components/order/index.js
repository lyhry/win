import React, { Component } from 'react'

import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  BackAndroid,
  BackHandler,
  ToastAndroid
} from "react-native";
import Icon from "react-native-vector-icons/AntDesign";

const Dimensions = require("Dimensions");
const { width,height } = Dimensions.get("window");


import { Tabs,WhiteSpace, WingBlank,Card,List,NoticeBar, Toast, Modal, Provider } from '@ant-design/react-native';
export default class BasicTabsExample extends React.Component {
  constructor(props) {
    super(props);
    this.info = this.info.bind(this)
    this.onclick = this.onclick.bind(this)
  }
  componentDidMount(){
    this.info()
  }
  componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackAndroid);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackAndroid);
  }
  onBackAndroid = () => {
    if (this.props.navigation.isFocused()) {
      // if (this.lastBackPressed && this.lastBackPressed + 2000 >= Date.now()) { 
      //  //最近2秒内按过back键，可以退出应用。
      //  return true; 
      // } 
      Toast.info('请耐心等待，稍后为你服务',2)
      // this.lastBackPressed = Date.now(); 
      // ToastAndroid.show('再按一次退出应用', ToastAndroid.SHORT); 
      // ToastAndroid.show('再按一次退出应用', ToastAndroid.SHORT); 
      // alert('请')
      return true; 
   }
  };
  info(){
    setTimeout(function(){
      Toast.info('支付成功')
    },1000)
  } 
  onclick = () =>{
    Toast.info('感谢你对我们评价👍')
  }
  render() {
    const { navigation } = this.props;
    const tableid = navigation.getParam('tableid', 'null');
    const tabletext = navigation.getParam('tabletext', 'null');
    const total = navigation.getParam('total', 'null');
    const allWine = navigation.getParam('allWine', 'null');
    const time = navigation.getParam('time', 'null');
    return (
      <Provider>
      <View style={styles.container}>
      <WhiteSpace></WhiteSpace>
      <NoticeBar
          onPress={() => alert('click')}
          marqueeProps={{ loop: true, style: { fontSize: 14, color: 'red' } }}
          icon={<Icon name='smileo' size={16} color='#00b894'></Icon>}
          style={{backgroundColor:"#F5FCFF",marginLeft:14}}
        >
          <Text style={{color:'#888'}}>请耐心等待，稍后会为你服务，祝您用餐愉快 请耐心等待，稍后会为你服务，祝您用餐愉快</Text>
          
        </NoticeBar>
      
        <WingBlank size="lg">
          <Card>
            <Card.Header
              title={total}
              thumbStyle={{ width: 30, height: 30 }}
              thumb={<Icon name='pay-circle1' size={22} color='#0984e3' style={{marginRight: 5}}></Icon>}
              extra={tabletext}
            />
            <Card.Body>
              <ScrollView style={{height:height/2-100}} showsVerticalScrollIndicator={false}>
                <List style={{ marginLeft: 16 ,marginRight:16,flex:1}}>
                 {
                   allWine.map((v,index) => {
                     return (
                       <View key={index}>
                       <List.Item key={index} thumb={v.img}>{v.name}</List.Item>
                       <List.Item extra={v.price * v.count}>
                       <View style={{flexDirection:'row',marginBottom:5}}>
                          <Text>数量: x{v.count}</Text>
                       </View>
                       <View style={{flexDirection:'row'}}>
                          <Text>单价: {v.price}</Text>
                       </View>
                       </List.Item>
                       </View>
                     )
                   })
                 }
                </List>
              </ScrollView>
            </Card.Body>
            <Card.Footer
              content={time}
              // extra={<Text style={{justifyContent}}>$87</Text>}
            />
          </Card>
        </WingBlank>
        <View style={{height:180,justifyContent:"space-around",alignItems:'center'}}>
        <Text style={{fontSize:16}}>Please comment on this service!</Text>
        <View style={{flexDirection:"row",justifyContent:"space-around",alignItems:'center',width:width-100}}>
        <TouchableOpacity onPress={this.onclick}>
        <Icon name='smileo' size={38} color='#00b894'></Icon>

        </TouchableOpacity>
        <TouchableOpacity onPress={this.onclick}>
        <Icon name='frowno' size={38} color='#ff7675'></Icon>

        </TouchableOpacity>
        </View>

        </View>
      </View>
      </Provider>
     
    );
  }
}
const styles = StyleSheet.create({
  container:{
    flex: 1,
    backgroundColor: "#F5FCFF",
    // justifyContent: "center",
    // alignItems: "center"
  },
  header: {
        height: width / 6.5,
        width: width/1.03,
        backgroundColor: "#F5FCFF",
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "row",
        position: "absolute"
      },
      body: {
      marginTop: width /6.5,
      flex:1,
  },

})